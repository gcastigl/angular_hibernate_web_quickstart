package com.gcastigl.joggingtracker.test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.Role.RoleType;
import com.gcastigl.joggingtracker.service.api.UserService.UserServiceErrorType;
import com.gcastigl.joggingtracker.util.Either;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@Transactional
@DatabaseSetup("classpath:/dataset/test-data.xml")
public class UserTest extends AppTest {

	@Test
	public void createRegularUser() {
		String username = "pepe";
		Either<UserServiceErrorType, Appuser> result = app.users().create(Optional.empty(), username, "12345", RoleType.REGULAR);
		assertTrue(result.isRight());
		assertTrue(app.users().findByUsername(username).isPresent());
	}

	@Test
	public void regularUsersCannotCreateAdmin() {
		Appuser foo = app.users().findByUsername("foo").get();
		assertFalse(app.users().isAdmin(foo));
		Either<UserServiceErrorType, Appuser> result = app.users().create(Optional.of(foo), "test", "12345", RoleType.ADMIN);
		assertTrue(!result.isRight());
		assertThat(result.get(), equalTo(UserServiceErrorType.NOT_ALLOWED));
		assertTrue(!app.users().findByUsername("test").isPresent());
	}

	@Test
	public void adminUsersCanCreateAdmins() {
		Appuser admin = app.users().findByUsername("admin").get();
		assertTrue(app.users().isAdmin(admin));
		Either<UserServiceErrorType, Appuser> result = app.users().create(Optional.of(admin), "test", "12345", RoleType.ADMIN);
		assertTrue(result.isRight());
		assertTrue(app.users().findByUsername("test").isPresent());
	}

}
