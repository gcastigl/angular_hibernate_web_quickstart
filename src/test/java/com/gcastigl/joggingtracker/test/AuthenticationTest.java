package com.gcastigl.joggingtracker.test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AuthenticationToken;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@Transactional
@DatabaseSetup("classpath:/dataset/test-data.xml")
public class AuthenticationTest extends AppTest {

	@Test
	public void testTokenAuth() {
		String username = "foo";
		AuthenticationToken authToken = app.authentication().createToken(username, "1", Optional.empty());
		assertThat(authToken.getUsername(), equalTo(username));
		assertThat(authToken.getToken(), notNullValue());
		assertTrue(authToken.getExpiration() > System.currentTimeMillis());
		Appuser auth = app.authentication().getUserFromToken(authToken.getToken()).get();
		assertTrue(auth.getUsername().equals(username));
	}

	@Test
	public void testExpiredToken() {
		String username = "foo";
		AuthenticationToken authToken = app.authentication().createToken(username, "1", Optional.of(-1L));
		Optional<Appuser> auth = app.authentication().getUserFromToken(authToken.getToken());
		assertFalse(auth.isPresent());
	}
}
