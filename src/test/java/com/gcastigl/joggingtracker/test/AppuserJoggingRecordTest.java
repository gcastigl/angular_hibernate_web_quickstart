package com.gcastigl.joggingtracker.test;

import static jersey.repackaged.com.google.common.collect.Iterables.getOnlyElement;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AppuserJoggingRecord;
import com.gcastigl.joggingtracker.service.api.AppuserJoggingRecordService.AppuserJoggingRecordErrorType;
import com.gcastigl.joggingtracker.util.Either;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@Transactional
@DatabaseSetup("classpath:/dataset/test-data.xml")
public class AppuserJoggingRecordTest extends AppTest {

	@Test
	public void regularUserCannotEditOthersRecords() {
		Appuser bar = app.users().findByUsername("bar").get();
		AppuserJoggingRecord barsRecord = getOnlyElement(app.joggingRecords().findAll(bar, Optional.empty(), Optional.empty(), Optional.empty(), new PageRequest(0, 1)));
		Appuser foo = app.users().findByUsername("foo").get();
		assertTrue(!app.users().isAdmin(foo));
		Either<AppuserJoggingRecordErrorType, AppuserJoggingRecord>  result = app.joggingRecords().update(foo, barsRecord, LocalDate.now(), 1, 1, null);
		assertTrue(!result.isRight());
		assertThat(result.get(), equalTo(AppuserJoggingRecordErrorType.NOT_ALLOWED));
	}

	@Test
	public void regularUserCannotSeeOtherRecords() {
		Appuser foo = app.users().findByUsername("foo").get();
		Appuser bar = app.users().findByUsername("bar").get();
		assertTrue(!app.users().isAdmin(foo));
		Page<AppuserJoggingRecord> foosRecordsByFoo = app.joggingRecords().findAll(foo, Optional.empty(), Optional.empty(), Optional.of("foo"), new PageRequest(0, 1));
		Page<AppuserJoggingRecord> foosRecordsByBar = app.joggingRecords().findAll(bar, Optional.empty(), Optional.empty(), Optional.of("foo"), new PageRequest(0, 1));
		assertTrue(foosRecordsByFoo.hasContent() && !foosRecordsByBar.hasContent());
	}

	@Test
	public void adminUserCanEditOthersRecords() {
		Appuser bar = app.users().findByUsername("bar").get();
		AppuserJoggingRecord barsRecord = getOnlyElement(app.joggingRecords().findAll(bar, Optional.empty(), Optional.empty(), Optional.empty(), new PageRequest(0, 1)));
		Appuser admin = app.users().findByUsername("admin").get();
		assertTrue(app.users().isAdmin(admin));
		Either<AppuserJoggingRecordErrorType, AppuserJoggingRecord>  result = app.joggingRecords().update(admin, barsRecord, LocalDate.now(), 1, 1, Optional.empty());
		assertTrue(result.isRight());
	}

	@Test
	public void adminUserCantSeeOthersRecords() {
		Appuser admin = app.users().findByUsername("admin").get();
		Appuser bar = app.users().findByUsername("bar").get();
		assertTrue(app.users().isAdmin(admin));
		Page<AppuserJoggingRecord> foosRecordsByAdmin = app.joggingRecords().findAll(admin, Optional.empty(), Optional.empty(), Optional.of("bar"), new PageRequest(0, 1));
		Page<AppuserJoggingRecord> foosRecordsByBar = app.joggingRecords().findAll(bar, Optional.empty(), Optional.empty(), Optional.of("bar"), new PageRequest(0, 1));
		assertTrue(foosRecordsByAdmin.hasContent() && foosRecordsByBar.hasContent());
		assertTrue(foosRecordsByAdmin.getTotalElements() == foosRecordsByBar.getTotalElements());
	}

}
