package com.gcastigl.joggingtracker.test;

import static org.junit.Assert.assertTrue;

import javax.persistence.PersistenceException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateJdbcException;
import org.springframework.transaction.annotation.Transactional;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.Role;
import com.gcastigl.joggingtracker.entity.Role.RoleType;
import com.gcastigl.joggingtracker.repository.RoleRepository;
import com.gcastigl.joggingtracker.service.api.PlatformTransactionService;
import com.gcastigl.joggingtracker.service.api.UserService;
import com.github.springtestdbunit.annotation.DatabaseSetup;

public class TransactionPlatformTest extends AppTest {

	@Autowired
	private PlatformTransactionService transactions;

	@Autowired
	private UserService users;

	@Autowired
	private RoleRepository roleRepository;

	@Test
	@Transactional
	@DatabaseSetup("classpath:/dataset/test-data.xml")
	public void testSave() {
		long count = users.findAll(null).getTotalElements();
		transactions.executeVoid(status -> {
			Role role = app.users().findRole(RoleType.ADMIN);
			users.save(new Appuser("test", "", role));
		});
		assertTrue(count + 1 == users.findAll(null).getTotalElements());
	}

	@Test(expected = HibernateJdbcException.class)
	public void testReadonly() {
		transactions.readonlyVoid(status -> {
			roleRepository.save(new Role(RoleType.ADMIN));
		});
		assertTrue("Readonly transaction successfully executed save operation", false);
	}
}
