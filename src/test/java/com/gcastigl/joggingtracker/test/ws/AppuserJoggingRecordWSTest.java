package com.gcastigl.joggingtracker.test.ws;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.test.AppTest;
import com.gcastigl.joggingtracker.web.pojo.AuthenticationRequestPojo;
import com.gcastigl.joggingtracker.web.pojo.AuthenticationResponse;
import com.gcastigl.joggingtracker.web.service.AuthenticationResource;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@Transactional
@DatabaseSetup("classpath:/dataset/test-data.xml")
public class AppuserJoggingRecordWSTest extends AppTest {

	@Autowired
	private AuthenticationResource authResource;

	@Test
	public void testTokenAuth() {
		AuthenticationRequestPojo request = new AuthenticationRequestPojo();
		request.setUsername("admin");
		request.setPassword("1");
		AuthenticationResponse response = authResource.authenticate(request);
		Appuser admin = app.authentication().getUserFromToken(response.getToken()).get();
		assertEquals(admin.getUsername(), request.getUsername());
	}

}
