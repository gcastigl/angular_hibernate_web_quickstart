package com.gcastigl.joggingtracker.test;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AppuserJoggingWeekReport;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@Transactional
@DatabaseSetup("classpath:/dataset/test-data.xml")
public class AppuserJoggingRecordReportTest extends AppTest {

	@Test
	public void regularUserCannotSeeOthersReport() {
		Appuser foo = app.users().findByUsername("foo").get();
		Appuser bar = app.users().findByUsername("bar").get();
		assertTrue(!app.users().isAdmin(foo));
		Page<AppuserJoggingWeekReport> foosReportByFoo = app.joggingRecords().getWeeklyReport(foo, Optional.empty(), Optional.empty(), Optional.of("foo"), new PageRequest(0, 1));
		Page<AppuserJoggingWeekReport> foosReportByBar = app.joggingRecords().getWeeklyReport(bar, Optional.empty(), Optional.empty(), Optional.of("foo"), new PageRequest(0, 1));
		assertTrue(foosReportByFoo.hasContent() && !foosReportByBar.hasContent());
	}

	@Test
	public void adminUserCantSeeOthersReport() {
		Appuser admin = app.users().findByUsername("admin").get();
		Appuser bar = app.users().findByUsername("bar").get();
		assertTrue(app.users().isAdmin(admin));
		Page<AppuserJoggingWeekReport> foosReportByAdmin = app.joggingRecords().getWeeklyReport(admin, Optional.empty(), Optional.empty(), Optional.of("bar"), new PageRequest(0, 1));
		Page<AppuserJoggingWeekReport> foosReportByBar = app.joggingRecords().getWeeklyReport(bar, Optional.empty(), Optional.empty(), Optional.of("bar"), new PageRequest(0, 1));
		assertTrue(foosReportByAdmin.hasContent() && foosReportByBar.hasContent());
		assertTrue(foosReportByAdmin.getTotalElements() == foosReportByBar.getTotalElements());
	}

}
