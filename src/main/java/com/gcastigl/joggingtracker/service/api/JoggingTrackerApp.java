package com.gcastigl.joggingtracker.service.api;

public interface JoggingTrackerApp {

	UserService users();

	AuthenticationService authentication();

	AppuserJoggingRecordService joggingRecords();

	PlatformTransactionService transactions();

}
