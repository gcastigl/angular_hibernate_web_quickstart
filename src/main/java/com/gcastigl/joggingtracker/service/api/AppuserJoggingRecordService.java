package com.gcastigl.joggingtracker.service.api;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AppuserJoggingRecord;
import com.gcastigl.joggingtracker.entity.AppuserJoggingWeekReport;
import com.gcastigl.joggingtracker.util.Either;

public interface AppuserJoggingRecordService extends GenericService<AppuserJoggingRecord, Long> {

	enum AppuserJoggingRecordErrorType {
		INVALID_DISTANCE,
		INVALID_DURATION,
		INVALID_COMMENT,
		NOT_ALLOWED
	}

	Either<AppuserJoggingRecordErrorType, AppuserJoggingRecord> create(Appuser user, LocalDate date, float distance, float duration, Optional<String> comment);

	Either<AppuserJoggingRecordErrorType, AppuserJoggingRecord> update(Appuser user, AppuserJoggingRecord record, LocalDate date, float distance, float duration, Optional<String> comment);

	Page<AppuserJoggingRecord> findAll(Appuser user, Optional<LocalDate> dateFrom, Optional<LocalDate> dateTo, Optional<String> username, Pageable pageable);

	boolean delete(Appuser user, long id);

	Page<AppuserJoggingWeekReport> getWeeklyReport(Appuser user, Optional<LocalDate> from, Optional<LocalDate> to, Optional<String> username, Pageable pageable);

}
