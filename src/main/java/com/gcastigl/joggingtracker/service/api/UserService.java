package com.gcastigl.joggingtracker.service.api;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AppuserSettings;
import com.gcastigl.joggingtracker.entity.Role;
import com.gcastigl.joggingtracker.entity.Role.RoleType;
import com.gcastigl.joggingtracker.util.Either;

public interface UserService extends GenericService<Appuser, Long> {

	public enum UserServiceErrorType {
		USERNAME_INVALID,
		USERNAME_EXISTS, 
		USERNAME_NOT_EXISTS, 
		PASSWORD_MISSMATCH,
		PASSWORD_WEAK, 
		PASSWORD_INCORRECT,
		CANT_EDIT_SELF, 
		NOT_ALLOWED,
		ROLE_INVALID
		;
	}

	Either<UserServiceErrorType, Appuser> create(Optional<Appuser> user, String username, String rawPassword, RoleType roleType);

	Page<Appuser> findAll(Appuser user, Pageable pageable);

	Optional<Appuser> findByUsername(String username);

	Role findRole(RoleType roleType);

	AppuserSettings getSettingsFor(String username);

	String getEncodedPassword(String username);

	boolean isAdmin(Appuser user);

	Optional<UserServiceErrorType> changePassword(Appuser user, String username, String currentPassword, String newPassword);

	Optional<UserServiceErrorType> update(Appuser user, String username, Optional<Boolean> enabled, Optional<RoleType> roleType);

}
