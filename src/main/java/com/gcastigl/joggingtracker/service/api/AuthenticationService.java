package com.gcastigl.joggingtracker.service.api;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AuthenticationToken;

public interface AuthenticationService extends UserDetailsService {

	enum AuthenticationErrorType {
		INVALID_CREDENTIALS,
		USER_DISABLED,
		INVALID_TOKEN
	}

	AuthenticationToken createToken(String username, String password, Optional<Long> hoursToExpiration);

	AuthenticationToken createToken(Appuser user, Optional<Long> hoursToExpiration);

	Optional<Appuser> getUserFromToken(String token);

}
