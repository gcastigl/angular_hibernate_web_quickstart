package com.gcastigl.joggingtracker.service.jpa;

import static com.gcastigl.joggingtracker.util.StringUtil.nullToEmpty;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AppuserSettings;
import com.gcastigl.joggingtracker.entity.Role;
import com.gcastigl.joggingtracker.entity.Role.RoleType;
import com.gcastigl.joggingtracker.repository.AppuserSettingsRepository;
import com.gcastigl.joggingtracker.repository.RoleRepository;
import com.gcastigl.joggingtracker.repository.UserRepository;
import com.gcastigl.joggingtracker.service.api.UserService;
import com.gcastigl.joggingtracker.util.Either;

@Service
public class UserServiceImpl extends JPAServiceImpl<Appuser, Long> implements UserService {

	private static final int MIN_USERNAME_LEN = 4;
	private static final int MIN_PASSWORD_LEN = 4;

	@Autowired
	private UserRepository repository_;

	@Autowired
	private RoleRepository rolesRepository_;

	@Autowired
	private AppuserSettingsRepository settingsRepository_;

	@Autowired
	private PasswordEncoder encoder_;

	@Override
	protected JpaRepository<Appuser, Long> repository() {
		return repository_;
	}

	@Override
	public Either<UserServiceErrorType, Appuser> create(Optional<Appuser> user, String username, String rawPassword, RoleType roleType) {
		username = username.toLowerCase();
		if (username.length() < MIN_USERNAME_LEN) {
			return Either.left(UserServiceErrorType.USERNAME_INVALID);
		}
		if (findByUsername(username).isPresent()) {
			return Either.left(UserServiceErrorType.USERNAME_EXISTS);
		}
		if (!isStrongPassword(rawPassword)) {
			return Either.left(UserServiceErrorType.PASSWORD_WEAK);
		}
		if (roleType == RoleType.ADMIN && (!user.isPresent() || !isAdmin(user.get()))) {
			return Either.left(UserServiceErrorType.NOT_ALLOWED);
		}
		return Either.right(save(new Appuser(username, encoder_.encode(rawPassword), findRole(roleType))));
	}

	@Override
	public Page<Appuser> findAll(Appuser user, Pageable pageable) {
		return isAdmin(user) ? findAll(pageable) : new PageImpl<>(Collections.emptyList());
	}

	@Override
	public Optional<Appuser> findByUsername(String username) {
		return Optional.ofNullable(repository_.findByUsername(nullToEmpty(username).toLowerCase()));
	}

	@Override
	public Role findRole(RoleType roleType) {
		return rolesRepository_.findByName(roleType);
	}

	@Override
	public AppuserSettings getSettingsFor(String username) {
		return Objects.requireNonNull(settingsRepository_.findByUsername(nullToEmpty(username).toLowerCase()));
	}

	@Override
	public String getEncodedPassword(String username) {
		return repository_.findPasswordByUsername(nullToEmpty(username).toLowerCase());
	}

	@Override
	public boolean isAdmin(Appuser user) {
		return user.getRole().getName().equals(RoleType.ADMIN);
	}

	private boolean isStrongPassword(String password) {
		return password != null && password.length() > MIN_PASSWORD_LEN;
	}

	@Override
	public Optional<UserServiceErrorType> changePassword(Appuser user, String username, String currentPassword, String newPassword) {
		if (!isStrongPassword(newPassword)) {
			return Optional.of(UserServiceErrorType.PASSWORD_WEAK);
		}
		username = nullToEmpty(username).toLowerCase();
		if (StringUtils.isEmpty(username) || user.getUsername().equals(username)) {
			if (!encoder_.matches(currentPassword, user.getPassword())) {
				return Optional.of(UserServiceErrorType.PASSWORD_INCORRECT);
			}
			user.setPassword(encoder_.encode(newPassword));
			save(user);
		} else if (isAdmin(user)) {
			Optional<Appuser> other = findByUsername(username);
			if (!other.isPresent()) {
				return Optional.of(UserServiceErrorType.USERNAME_NOT_EXISTS);
			}
			other.get().setPassword(newPassword);
			save(other.get());
		}
		return Optional.empty();
	}

	@Override
	public Optional<UserServiceErrorType> update(Appuser user, String username, Optional<Boolean> enabled, Optional<RoleType> roleType) {
		if (isAdmin(user)) {
			Optional<Appuser> userToUpdate = findByUsername(username);
			if (!userToUpdate.isPresent()) {
				return Optional.of(UserServiceErrorType.USERNAME_NOT_EXISTS);
			}
			if (userToUpdate.get().getUsername().equals(user.getUsername())) {
				return Optional.of(UserServiceErrorType.CANT_EDIT_SELF);
			}
			if (enabled.isPresent()) {
				userToUpdate.get().setEnabled(enabled.get());
			}
			if (roleType.isPresent()) {
				userToUpdate.get().setRole(rolesRepository_.findByName(roleType.get()));
			}
			save(userToUpdate.get());
			return Optional.empty();
		}
		return Optional.of(UserServiceErrorType.NOT_ALLOWED);
	}
}
