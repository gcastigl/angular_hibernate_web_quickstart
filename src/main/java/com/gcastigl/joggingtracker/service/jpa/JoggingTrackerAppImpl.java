package com.gcastigl.joggingtracker.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcastigl.joggingtracker.service.api.AppuserJoggingRecordService;
import com.gcastigl.joggingtracker.service.api.AuthenticationService;
import com.gcastigl.joggingtracker.service.api.JoggingTrackerApp;
import com.gcastigl.joggingtracker.service.api.PlatformTransactionService;
import com.gcastigl.joggingtracker.service.api.UserService;

@Service
public class JoggingTrackerAppImpl implements JoggingTrackerApp {

	@Autowired
	private PlatformTransactionService transactions;

	@Autowired
	private UserService users;

	@Autowired
	private AuthenticationService authentication;

	@Autowired
	private AppuserJoggingRecordService joggingRecords;

	@Override
	public UserService users() {
		return users;
	}

	@Override
	public AuthenticationService authentication() {
		return authentication;
	}

	@Override
	public AppuserJoggingRecordService joggingRecords() {
		return joggingRecords;
	}

	@Override
	public PlatformTransactionService transactions() {
		return transactions;
	}

}
