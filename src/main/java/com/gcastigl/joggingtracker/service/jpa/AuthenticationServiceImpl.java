package com.gcastigl.joggingtracker.service.jpa;

import static java.util.Collections.singletonList;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AuthenticationToken;
import com.gcastigl.joggingtracker.entity.Role;
import com.gcastigl.joggingtracker.entity.Role.RoleType;
import com.gcastigl.joggingtracker.service.api.AuthenticationService;
import com.gcastigl.joggingtracker.service.api.UserService;

import jersey.repackaged.com.google.common.collect.Iterables;

@Service("userDetailsService")
public class AuthenticationServiceImpl implements AuthenticationService {

	@Autowired
	private UserService users;

	@Value("${token.secret}")
	private String secret_;

	@Value("${token.hoursToExpiration}")
	private Long hoursToExpiration_;

	@Autowired
	private PasswordEncoder encoder;

	private final String SEPARATOR = ":";

	private final AccountStatusUserDetailsChecker accountChecker = new AccountStatusUserDetailsChecker();

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Appuser> user = users.findByUsername(username);
		if (!user.isPresent()) {
			throw new UsernameNotFoundException(username);
		}
		return buildUserDetails(user.get());
	}

	private UserDetails buildUserDetails(Appuser user) {
		String username = user.getUsername();
		String password = user.getPassword();
		boolean enabled = user.isEnabled();
		Role role = user.getRole();
		List<GrantedAuthority> authorities = singletonList(new SimpleGrantedAuthority(role.getName().toString()));
		return new User(username, password, enabled, true, true, true, authorities);
	}

	@Override
	public AuthenticationToken createToken(String username, String rawPassword, Optional<Long> hoursToExpiration) {
		UserDetails userdetails = validateUserDetails(loadUserByUsername(username), rawPassword);
		Long expiration = expiration(hoursToExpiration);
		String token = generateToken(username, userdetails.getPassword(), expiration);
		String authority = Iterables.getOnlyElement(userdetails.getAuthorities()).getAuthority();
		return new AuthenticationToken(username, expiration, RoleType.valueOf(authority), token);
	}

	private Long expiration(Optional<Long> hoursToExpiration) {
		return System.currentTimeMillis() + TimeUnit.HOURS.toMillis(hoursToExpiration.orElse(hoursToExpiration_));
	}

	private UserDetails validateUserDetails(UserDetails user, String rawPassword) {
		accountChecker.check(user);
		if (!encoder.matches(rawPassword, user.getPassword())) {
			throw new BadCredentialsException("Invalid password");
		}
		return user;
	}

	private String generateToken(String username, String password, Long expiration) {
		StringBuilder token = new StringBuilder();
		token.append(username).append(SEPARATOR);
		token.append(expiration).append(SEPARATOR);
		token.append(encoder.encode(genRawSignature(username, password, expiration)));
		return token.toString();
	}

	private String genRawSignature(String username, String password, Long expiration) {
		StringBuilder signature = new StringBuilder();
		signature.append(username).append(SEPARATOR);
		signature.append(password).append(SEPARATOR);
		signature.append(expiration).append(SEPARATOR);
		signature.append(secret_);
		return signature.toString();
	}

	@Override
	public Optional<Appuser> getUserFromToken(String token) {
		if (!isValidToken(token)) {
			return Optional.empty();
		}
		return users.findByUsername(token.split(SEPARATOR)[0]);
	}

	private boolean isValidToken(String token) {
		if (token == null) {
			return false;
		}
		String[] parts = token.split(SEPARATOR);
		if (parts.length != 3) {
			return false;
		}
		try {
			String username = parts[0];
			Long expiration = Long.parseLong(parts[1]);
			String signature = parts[2];
			if (expiration <= System.currentTimeMillis()) {
				return false;
			}
			String password = users.getEncodedPassword(username);
			return encoder.matches(genRawSignature(username, password, expiration), signature);
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public AuthenticationToken createToken(Appuser user, Optional<Long> hoursToExpiration) {
		Long expiration = expiration(hoursToExpiration);
		String token = generateToken(user.getUsername(), user.getPassword(), expiration);
		return new AuthenticationToken(user.getUsername(), expiration, user.getRole().getName(), token);
	}
}
