package com.gcastigl.joggingtracker.service.jpa;

import java.time.LocalDate;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AppuserJoggingRecord;
import com.gcastigl.joggingtracker.entity.AppuserJoggingWeekReport;
import com.gcastigl.joggingtracker.repository.AppuserJoggingRecordRepository;
import com.gcastigl.joggingtracker.service.api.AppuserJoggingRecordService;
import com.gcastigl.joggingtracker.service.api.UserService;
import com.gcastigl.joggingtracker.util.Either;

@Service
public class AppuserJoggingRecordServiceImpl extends JPAServiceImpl<AppuserJoggingRecord, Long> implements AppuserJoggingRecordService {

	@Autowired
	private AppuserJoggingRecordRepository repository_;

	@Autowired
	private UserService users_;

	@PersistenceContext
	private EntityManager em_;

	@Override
	protected JpaRepository<AppuserJoggingRecord, Long> repository() {
		return repository_;
	}

	@Override
	public Either<AppuserJoggingRecordErrorType, AppuserJoggingRecord> create(Appuser user, LocalDate date, float distance, float duration, Optional<String> comment) {
		Optional<AppuserJoggingRecordErrorType> error = validate(date, distance, duration, comment);
		if (error.isPresent()) {
			return Either.left(error.get());
		}
		return Either.right(save(new AppuserJoggingRecord(user, date, distance, duration, comment)));
	}

	private Optional<AppuserJoggingRecordErrorType> validate(LocalDate date, float distance, float duration, Optional<String> comment) {
		if (distance <= 0) {
			return Optional.of(AppuserJoggingRecordErrorType.INVALID_DISTANCE);
		}
		if (duration <= 0) {
			return Optional.of(AppuserJoggingRecordErrorType.INVALID_DURATION);
		}
		if (comment.isPresent() && comment.get().length() >= AppuserJoggingRecord.COMMENT_MAX_LEN) {
			return Optional.of(AppuserJoggingRecordErrorType.INVALID_COMMENT);
		}
		return Optional.empty();
	}

	@Override
	public Either<AppuserJoggingRecordErrorType, AppuserJoggingRecord> update(Appuser user, AppuserJoggingRecord record, LocalDate date, float distance, float duration, Optional<String> comment) {
		if (!canEdit(user, record)) {
			return Either.left(AppuserJoggingRecordErrorType.NOT_ALLOWED);
		}
		Optional<AppuserJoggingRecordErrorType> error = validate(date, distance, duration, comment);
		if (error.isPresent()) {
			return Either.left(error.get());
		}
		record.setDate(date);
		record.setDistance(distance);
		record.setDuration(duration);
		record.setComment(comment.orElse(null));
		return Either.right(save(record));
	}

	@Override
	public Page<AppuserJoggingRecord> findAll(Appuser user, Optional<LocalDate> dateFrom, Optional<LocalDate> dateTo, Optional<String> username, Pageable pageable) {
		Appuser userToFilter = users_.isAdmin(user) ? null : user;
		String usernameFilter = username.isPresent() ? String.format("%%%s%%", username.get()) : null;
		return repository_.findAllByUser(userToFilter, dateFrom.orElse(null), dateTo.orElse(null), usernameFilter, pageable);
	}

	@Override
	public boolean delete(Appuser user, long id) {
		Optional<AppuserJoggingRecord> record = findOne(id);
		if (!record.isPresent()) {
			return false;
		}
		if (!canEdit(user, record.get())) {
			return false;
		}
		delete(id);
		return true;
	}

	@Override
	public Page<AppuserJoggingWeekReport> getWeeklyReport(Appuser user, Optional<LocalDate> from, Optional<LocalDate> to, Optional<String> username, Pageable pageable) {
		Appuser userToFilter = users_.isAdmin(user) ? null : user;
		String usernameFilter = username.isPresent() ? String.format("%%%s%%", username.get()) : null;
		return repository_.getWeeklyReport(userToFilter, from.orElse(null), to.orElse(null), usernameFilter, pageable);
	}

	private boolean canEdit(Appuser user, AppuserJoggingRecord record) {
		return users_.isAdmin(user) || isOwner(user, record);
	}

	private boolean isOwner(Appuser user, AppuserJoggingRecord record) {
		return record.getUser().equals(user);
	}

}
