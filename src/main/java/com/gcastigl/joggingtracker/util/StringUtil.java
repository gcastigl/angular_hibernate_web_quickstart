package com.gcastigl.joggingtracker.util;

public class StringUtil {

	public static String firstToUpperCase(String s) {
		if (s == null || s.isEmpty()) {
			return s;
		}
		if (s.length() == 1) {
			s.substring(0, 1).toUpperCase();
		}
		s = s.toLowerCase();
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

	public static String nullToEmpty(String s) {
		return s == null ? "" : s;
	}
}
