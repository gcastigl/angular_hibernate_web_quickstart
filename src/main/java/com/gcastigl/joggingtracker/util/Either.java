package com.gcastigl.joggingtracker.util;

import java.util.function.Function;

import jersey.repackaged.com.google.common.base.Preconditions;

@SuppressWarnings("unchecked")
public abstract class Either<L, R> {

	public static final <L, R> Either<L, R> left(L value) {
		return new Left<L, R>(value);
	}

	public static final <L, R> Either<L, R> right(R value) {
		return new Right<L, R>(value);
	}

	public abstract <T> T get();

	public <T> Either<L, T> map(Function<R, T> f) {
		Preconditions.checkArgument(this instanceof Right);
		return right(f.apply(get()));
	}

	public abstract boolean isRight();

	static final class Left<L, R> extends Either<L, R> {

		public L value;

		public Left(L value) {
			this.value = value;
		}

		@Override
		public <T> T get() {
			return (T) value;
		}

		@Override
		public boolean isRight() {
			return false;
		}
	}

	static final class Right<L, R> extends Either<L, R> {

		public R value;

		public Right(R value) {
			this.value = value;
		}

		@Override
		public <T> T get() {
			return (T) value;
		}

		@Override
		public boolean isRight() {
			return true;
		}
	}

}
