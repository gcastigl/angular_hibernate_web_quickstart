package com.gcastigl.joggingtracker.web.pojo;

import javax.ws.rs.QueryParam;

public class AppuserJoggingRecordFilter extends PagerequestPojo {

	@QueryParam("dateFrom")
	private String dateFrom;

	@QueryParam("dateTo")
	private String dateTo;

	@QueryParam("username")
	private String username;

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}
}
