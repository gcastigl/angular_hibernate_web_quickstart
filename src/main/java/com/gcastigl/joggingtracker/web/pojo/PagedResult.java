package com.gcastigl.joggingtracker.web.pojo;

import java.util.List;

import org.springframework.data.domain.Page;

public class PagedResult<T> {

	private PageInfo pageInfo = new PageInfo();
	private List<T> content;

	public PagedResult(Page<T> page) {
		pageInfo.setTotalPages(page.getTotalPages());
		pageInfo.setTotalElements(page.getTotalElements());
		setContent(page.getContent());
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

	public List<T> getContent() {
		return content;
	}

	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}

	public PageInfo getPageInfo() {
		return pageInfo;
	}

	public static class PageInfo {
		private int totalPages;
		private long totalElements;

		public int getTotalPages() {
			return totalPages;
		}

		public void setTotalPages(int totalPages) {
			this.totalPages = totalPages;
		}

		public long getTotalElements() {
			return totalElements;
		}

		public void setTotalElements(long totalElements) {
			this.totalElements = totalElements;
		}

	}
}
