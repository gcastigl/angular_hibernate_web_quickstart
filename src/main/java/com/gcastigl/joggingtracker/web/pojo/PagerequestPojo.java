package com.gcastigl.joggingtracker.web.pojo;

import javax.ws.rs.QueryParam;

public class PagerequestPojo {

	@QueryParam("page")
	private int page;

	@QueryParam("pagesize")
	private int pagesize;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPagesize() {
		return pagesize;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

}
