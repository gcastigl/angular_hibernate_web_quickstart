package com.gcastigl.joggingtracker.web.pojo;

public class AppuserSettingsPojo {

	private int expectedWeeklySpeed;

	public void setExpectedWeeklySpeed(int expectedWeeklySpeed) {
		this.expectedWeeklySpeed = expectedWeeklySpeed;
	}

	public int getExpectedWeeklySpeed() {
		return expectedWeeklySpeed;
	}
}
