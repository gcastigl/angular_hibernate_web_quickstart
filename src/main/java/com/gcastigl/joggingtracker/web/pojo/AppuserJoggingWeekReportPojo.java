package com.gcastigl.joggingtracker.web.pojo;

public class AppuserJoggingWeekReportPojo {

	private int year;
	private int weekNumber;
	private float totalDistance;
	private float averageSpeed;
	private int joggingTimes;
	private String username;

	public void setYear(int year) {
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	public int getWeekNumber() {
		return weekNumber;
	}

	public void setWeekNumber(int weekNumber) {
		this.weekNumber = weekNumber;
	}

	public float getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(float totalDistance) {
		this.totalDistance = totalDistance;
	}

	public float getAverageSpeed() {
		return averageSpeed;
	}

	public void setAverageSpeed(float averageSpeed) {
		this.averageSpeed = averageSpeed;
	}

	public int getJoggingTimes() {
		return joggingTimes;
	}

	public void setJoggingTimes(int joggingTimes) {
		this.joggingTimes = joggingTimes;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

}
