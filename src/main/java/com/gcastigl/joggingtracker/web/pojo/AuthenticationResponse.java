package com.gcastigl.joggingtracker.web.pojo;

public class AuthenticationResponse {

	private String username;
	private Long expiration;
	private String token;
	private String role;
	private WSResponse result;

	public AuthenticationResponse(WSResponse result) {
		this.result = result;
	}

	public AuthenticationResponse(String username, Long expiration, String role, String token) {
		this.username = username;
		this.expiration = expiration;
		this.token = token;
		this.role = role;
		this.result = WSResponse.ok();
	}

	public void setResult(WSResponse result) {
		this.result = result;
	}

	public WSResponse getResult() {
		return result;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getExpiration() {
		return expiration;
	}

	public void setExpiration(Long expiration) {
		this.expiration = expiration;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getRole() {
		return role;
	}

}
