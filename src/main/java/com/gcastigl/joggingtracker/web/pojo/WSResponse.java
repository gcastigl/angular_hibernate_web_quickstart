package com.gcastigl.joggingtracker.web.pojo;

public class WSResponse {

	public static final WSResponse ok() {
		return new WSResponse(0, null);
	}

	public static final WSResponse error(Enum<?> enumvalue) {
		String name = String.format("%s_%s", enumvalue.getClass().getSimpleName(), enumvalue.name());
		return new WSResponse(enumvalue.ordinal() + 1, name);
	}

	public static final WSResponse error(int code, String message) {
		return new WSResponse(code, message);
	}

	/**
	 * <pre>
	 * 0 		=> OK
	 * != 0 	=> Error
	 * </pre>
	 */
	private int status;
	private String message;

	public WSResponse(int status, String message) {
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

}
