package com.gcastigl.joggingtracker.web.filter;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import com.gcastigl.joggingtracker.entity.Appuser;

public class AppSecurityContext implements SecurityContext {

	public static Appuser user(SecurityContext context) {
		UserPrincipal principal = (UserPrincipal) context.getUserPrincipal();
		return principal == null ? null : principal.getUser();
	}

	private Appuser user;
	private UserPrincipal principal;
	private String scheme;

	public AppSecurityContext(Appuser user, String scheme) {
		this.user = user;
		this.principal = new UserPrincipal(user);
		this.scheme = scheme;
	}

	@Override
	public Principal getUserPrincipal() {
		return principal;
	}

	@Override
	public boolean isUserInRole(String role) {
		return user.getRole().getName().name().equals(role);
	}

	@Override
	public boolean isSecure() {
		return "https".equals(scheme);
	}

	@Override
	public String getAuthenticationScheme() {
		return SecurityContext.BASIC_AUTH;
	}

	public Appuser getUser() {
		return user;
	}
}
