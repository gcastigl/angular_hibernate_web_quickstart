package com.gcastigl.joggingtracker.web.filter;

import java.io.IOException;
import java.util.Optional;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.service.api.JoggingTrackerApp;

public class AuthenticationFilter implements ContainerRequestFilter {

	public static final String HEADER_AUTH_TYPE = "Basic";

	@Autowired
	private JoggingTrackerApp app;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		boolean authRequired = requiresAuthentication(requestContext.getUriInfo().getPath().toLowerCase());
		setSecurityContext(requestContext, authRequired);
	}

	private boolean requiresAuthentication(String urlPath) {
		return !urlPath.equals("authenticate/token") && !urlPath.equals("users/create");
	}

	private void setSecurityContext(ContainerRequestContext request, boolean authRequired) {
		String authorizationHeader = request.getHeaderString(HttpHeaders.AUTHORIZATION);
		// Check if the HTTP Authorization header is present and formatted
		// correctly
		if (authorizationHeader == null || !authorizationHeader.startsWith(HEADER_AUTH_TYPE + " ")) {
			if (authRequired) {
				throw new NotAuthorizedException("Authorization header must be provided");
			}
			return;
		}
		// Extract the token from the HTTP Authorization header
		String token = authorizationHeader.substring(HEADER_AUTH_TYPE.length()).trim();
		try {
			Optional<Appuser> auth = app.authentication().getUserFromToken(token);
			if (!auth.isPresent()) {
				if (authRequired) {
					throw new NotAuthorizedException("Invalid authorization header");
				}
				return;
			}
			String scheme = request.getUriInfo().getRequestUri().getScheme();
			request.setSecurityContext(new AppSecurityContext(auth.get(), scheme));
		} catch (Exception e) {
			request.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}
	}

}
