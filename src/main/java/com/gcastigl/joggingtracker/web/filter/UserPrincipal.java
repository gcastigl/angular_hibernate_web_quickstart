package com.gcastigl.joggingtracker.web.filter;

import java.security.Principal;

import com.gcastigl.joggingtracker.entity.Appuser;

public class UserPrincipal implements Principal {

	private Appuser user;

	public UserPrincipal(Appuser user) {
		this.user = user;
	}

	@Override
	public String getName() {
		return user.getUsername();
	}

	public Appuser getUser() {
		return user;
	}

}
