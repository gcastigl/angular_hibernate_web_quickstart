package com.gcastigl.joggingtracker.web.filter;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.spi.ExtendedExceptionMapper;

import com.gcastigl.joggingtracker.service.api.AuthenticationService.AuthenticationErrorType;
import com.gcastigl.joggingtracker.web.pojo.AuthenticationResponse;
import com.gcastigl.joggingtracker.web.pojo.WSResponse;

public class ExceptionFilter implements ExtendedExceptionMapper<Throwable> {

	public enum WsErrorType {
		INVALID_PARAMETERS
	}

	@Override
	public Response toResponse(Throwable exception) {
		new AuthenticationResponse(WSResponse.error(AuthenticationErrorType.INVALID_CREDENTIALS));
		return Response.ok().entity(WSResponse.error(WsErrorType.INVALID_PARAMETERS)).build();
	}

	@Override
	public boolean isMappable(Throwable exception) {
		return !(exception instanceof WebApplicationException);
	}

}
