package com.gcastigl.joggingtracker.web.service;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AuthenticationToken;
import com.gcastigl.joggingtracker.entity.Role.RoleType;
import com.gcastigl.joggingtracker.service.api.JoggingTrackerApp;
import com.gcastigl.joggingtracker.service.api.UserService.UserServiceErrorType;
import com.gcastigl.joggingtracker.util.Either;
import com.gcastigl.joggingtracker.util.StringUtil;
import com.gcastigl.joggingtracker.web.filter.AppSecurityContext;
import com.gcastigl.joggingtracker.web.pojo.AuthenticationRequestPojo;
import com.gcastigl.joggingtracker.web.pojo.AuthenticationResponse;
import com.gcastigl.joggingtracker.web.pojo.ChangePasswordPojo;
import com.gcastigl.joggingtracker.web.pojo.CreateAppuserPojo;
import com.gcastigl.joggingtracker.web.pojo.UserPojo;
import com.gcastigl.joggingtracker.web.pojo.WSResponse;

import jersey.repackaged.com.google.common.base.Objects;

@Path("/users")
@Component
public class UserResource {

	@Autowired
	private JoggingTrackerApp app;

	@Autowired
	private AuthenticationResource authentication;
	
	@Autowired
	private ModelMapper mapper;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional(readOnly = true)
	public List<UserPojo> findAll(@Context SecurityContext securityContext) {
		Appuser user = AppSecurityContext.user(securityContext);
		return app.users().findAll(user, null).map(other -> {
			UserPojo pojo = mapper.map(other, UserPojo.class);
			pojo.setRole(StringUtil.firstToUpperCase(other.getRole().getName().name()));
			return pojo;
		}).getContent();
	}

	@POST
	@Path("create")
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public AuthenticationResponse create(CreateAppuserPojo request, @Context SecurityContext securityContext) {
		Appuser user = AppSecurityContext.user(securityContext);
		if (request.getPassword() == null || !Objects.equal(request.getPassword(), request.getPasswordConfirmation())) {
			return new AuthenticationResponse(WSResponse.error(UserServiceErrorType.PASSWORD_MISSMATCH));
		}
		Optional<RoleType> roleType = Optional.ofNullable(EnumUtils.getEnum(RoleType.class, StringUtils.upperCase(request.getRole())));
		if (!roleType.isPresent()) {
			return new AuthenticationResponse(WSResponse.error(UserServiceErrorType.ROLE_INVALID));
		}
		Either<UserServiceErrorType, Appuser> result = app.users().create(
			Optional.ofNullable(user), request.getUsername(), request.getPassword(), roleType.get()
		);
		if (result.isRight()) {
			AuthenticationRequestPojo authRequest = new AuthenticationRequestPojo();
			authRequest.setUsername(request.getUsername());
			authRequest.setPassword(request.getPassword());
			return authentication.authenticate(authRequest);
		}
		return new AuthenticationResponse(WSResponse.error(result.get()));
	}

	@POST
	@Path("update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public WSResponse update(UserPojo userPojo, @Context SecurityContext securityContext) {
		Appuser user = AppSecurityContext.user(securityContext);
		String roleTypeString = StringUtils.upperCase(userPojo.getRole());
		Optional<RoleType> roleType = Optional.ofNullable(EnumUtils.getEnum(RoleType.class, roleTypeString));
		if (roleTypeString != null && !roleType.isPresent()) {
			return WSResponse.error(UserServiceErrorType.ROLE_INVALID);
		}
		Optional<UserServiceErrorType>  errorType = app.users().update(
			user, userPojo.getUsername(), Optional.ofNullable(userPojo.isEnabled()), roleType
		);
		return errorType.isPresent() ? WSResponse.error(errorType.get()): WSResponse.ok();
	}

	@POST
	@Path("changepassword")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public AuthenticationResponse changePassword(ChangePasswordPojo pojo, @Context SecurityContext securityContext) {
		Appuser user = AppSecurityContext.user(securityContext);
		Optional<UserServiceErrorType> errorType = app.users().changePassword(
			user, pojo.getUsername(), pojo.getCurrentPassword(), pojo.getNewPassword()
		);
		if (errorType.isPresent()) {
			return new AuthenticationResponse(WSResponse.error(errorType.get()));
		}
		AuthenticationToken auth = app.authentication().createToken(pojo.getUsername(), pojo.getNewPassword(), Optional.empty());
		return new AuthenticationResponse(auth.getUsername(), auth.getExpiration(), auth.getRole().name().toLowerCase(), auth.getToken());
	}

}
