package com.gcastigl.joggingtracker.web.service;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.time.LocalDate;
import java.util.Optional;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AppuserJoggingRecord;
import com.gcastigl.joggingtracker.service.api.AppuserJoggingRecordService.AppuserJoggingRecordErrorType;
import com.gcastigl.joggingtracker.service.api.JoggingTrackerApp;
import com.gcastigl.joggingtracker.util.Either;
import com.gcastigl.joggingtracker.web.filter.AppSecurityContext;
import com.gcastigl.joggingtracker.web.pojo.AppuserJoggingRecordFilter;
import com.gcastigl.joggingtracker.web.pojo.AppuserJoggingRecordPojo;
import com.gcastigl.joggingtracker.web.pojo.AppuserJoggingWeekReportPojo;
import com.gcastigl.joggingtracker.web.pojo.PagedResult;
import com.gcastigl.joggingtracker.web.pojo.WSResponse;

@Path("/joggingrecords")
@Component
public class AppuserJoggingRecordResource {

	@Autowired
	private JoggingTrackerApp app;

	@Autowired
	private ModelMapper mapper;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional(readOnly = true)
	public PagedResult<AppuserJoggingRecordPojo> findAll(@BeanParam AppuserJoggingRecordFilter pageRequest, @Context SecurityContext securityContext) {
		Appuser user = AppSecurityContext.user(securityContext);
		Optional<LocalDate> from = Optional.ofNullable(isNotEmpty(pageRequest.getDateFrom()) ? LocalDate.parse(pageRequest.getDateFrom()) : null);
		Optional<LocalDate> to = Optional.ofNullable(isNotEmpty(pageRequest.getDateTo()) ? LocalDate.parse(pageRequest.getDateTo()) : null);
		Optional<String> username = Optional.ofNullable(pageRequest.getUsername());
		Pageable pageable = new PageRequest(pageRequest.getPage(), pageRequest.getPagesize(), new Sort(Direction.DESC, "date"));
		return new PagedResult<>(app.joggingRecords().findAll(user, from, to, username, pageable)
			.map(record -> {
				AppuserJoggingRecordPojo pojo = mapper.map(record, AppuserJoggingRecordPojo.class);
				pojo.setUsername(record.getUser().getUsername());
				return pojo;
			}));
	}

	@POST
	@Path("create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public WSResponse create(AppuserJoggingRecordPojo recordPojo, @Context SecurityContext securityContext) {
		Appuser user = AppSecurityContext.user(securityContext);
		LocalDate date = LocalDate.parse(recordPojo.getDate());
		float distance = recordPojo.getDistance();
		float duration = recordPojo.getDuration();
		Optional<String> comment = Optional.ofNullable(recordPojo.getComment());
		Either<AppuserJoggingRecordErrorType, AppuserJoggingRecord> result = app.joggingRecords().create(user, date, distance, duration, comment);
		if (result.isRight()) {
			return WSResponse.ok();
		}
		return WSResponse.error(result.get());
	}

	@POST
	@Path("update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public WSResponse update(AppuserJoggingRecordPojo recordPojo, @Context SecurityContext securityContext) {
		Appuser user = AppSecurityContext.user(securityContext);
		Optional<AppuserJoggingRecord> record = app.joggingRecords().findOne(recordPojo.getId());
		if (record.isPresent()) {
			LocalDate date = LocalDate.parse(recordPojo.getDate());
			float distance = recordPojo.getDistance();
			float duration = recordPojo.getDuration();
			Optional<String> comment = Optional.ofNullable(recordPojo.getComment());
			Either<AppuserJoggingRecordErrorType, AppuserJoggingRecord> result = app.joggingRecords().update(user, record.get(), date, distance, duration, comment);
			return result.isRight() ? WSResponse.ok() : WSResponse.error(result.get());
		}
		return WSResponse.error(AppuserJoggingRecordErrorType.NOT_ALLOWED);
	}

	@POST
	@Path("delete/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public WSResponse delete(@PathParam("id") long id, @Context SecurityContext securityContext) {
		Appuser user = AppSecurityContext.user(securityContext);
		boolean success = app.joggingRecords().delete(user, id);
		return success ? WSResponse.ok() : WSResponse.error(AppuserJoggingRecordErrorType.NOT_ALLOWED);
	}

	@GET
	@Path("report")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional(readOnly = true)
	public PagedResult<AppuserJoggingWeekReportPojo> weeklyReport(@BeanParam AppuserJoggingRecordFilter pageRequest, @Context SecurityContext securityContext) {
		Appuser user = AppSecurityContext.user(securityContext);
		Optional<LocalDate> from = Optional.ofNullable(isNotEmpty(pageRequest.getDateFrom()) ? LocalDate.parse(pageRequest.getDateFrom()) : null);
		Optional<LocalDate> to = Optional.ofNullable(isNotEmpty(pageRequest.getDateTo()) ? LocalDate.parse(pageRequest.getDateTo()) : null);
		Optional<String> username = Optional.ofNullable(pageRequest.getUsername());
		Pageable pageable = new PageRequest(pageRequest.getPage(), pageRequest.getPagesize());
		return new PagedResult<>(app.joggingRecords().getWeeklyReport(user, from, to, username, pageable)
			.map(record ->  mapper.map(record, AppuserJoggingWeekReportPojo.class))
		);
	}
}
