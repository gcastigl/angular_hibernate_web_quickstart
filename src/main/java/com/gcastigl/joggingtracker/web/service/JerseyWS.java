package com.gcastigl.joggingtracker.web.service;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.glassfish.jersey.server.ResourceConfig;

import com.gcastigl.joggingtracker.web.filter.AuthenticationFilter;
import com.gcastigl.joggingtracker.web.filter.ExceptionFilter;

public class JerseyWS extends ResourceConfig {

	public JerseyWS() {
		register(AuthenticationFilter.class);
		register(ExceptionFilter.class);
		register(AuthenticationResource.class);
		register(UserResource.class);
		register(UserSettingsResource.class);
		register(AppuserJoggingRecordResource.class);
		register(new JacksonJsonProvider());
	}

}
