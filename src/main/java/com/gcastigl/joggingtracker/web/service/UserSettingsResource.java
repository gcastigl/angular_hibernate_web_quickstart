package com.gcastigl.joggingtracker.web.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcastigl.joggingtracker.entity.AppuserSettings;
import com.gcastigl.joggingtracker.service.api.JoggingTrackerApp;
import com.gcastigl.joggingtracker.web.pojo.AppuserSettingsPojo;

@Path("/settings")
@Component
public class UserSettingsResource {

	@Autowired
	private JoggingTrackerApp app;

	@Autowired
	private ModelMapper mapper;

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public AppuserSettingsPojo get(@Context SecurityContext securityContext) {
		AppuserSettings settings = app.users().getSettingsFor(securityContext.getUserPrincipal().getName());
		return mapper.map(settings, AppuserSettingsPojo.class);
	}

}
