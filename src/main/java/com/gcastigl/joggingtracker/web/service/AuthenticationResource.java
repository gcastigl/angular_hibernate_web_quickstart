package com.gcastigl.joggingtracker.web.service;

import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AuthenticationToken;
import com.gcastigl.joggingtracker.service.api.AuthenticationService.AuthenticationErrorType;
import com.gcastigl.joggingtracker.service.api.JoggingTrackerApp;
import com.gcastigl.joggingtracker.util.StringUtil;
import com.gcastigl.joggingtracker.web.filter.AppSecurityContext;
import com.gcastigl.joggingtracker.web.pojo.AuthenticationRequestPojo;
import com.gcastigl.joggingtracker.web.pojo.AuthenticationResponse;
import com.gcastigl.joggingtracker.web.pojo.WSResponse;

@Path("/authenticate")
@Component
public class AuthenticationResource {

	@Autowired
	private JoggingTrackerApp app;

	@POST
	@Path("token")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional(readOnly = true)
	public AuthenticationResponse authenticate(final AuthenticationRequestPojo request) {
		try {
			return response(app.authentication().createToken(request.getUsername(), request.getPassword(), Optional.empty()));
		} catch (DisabledException e) {
			return new AuthenticationResponse(WSResponse.error(AuthenticationErrorType.USER_DISABLED));
		} catch (AuthenticationException ex) {
			return new AuthenticationResponse(WSResponse.error(AuthenticationErrorType.INVALID_CREDENTIALS));
		}
	}

	@POST
	@Path("renewtoken")
	@Transactional(readOnly = true)
	public AuthenticationResponse renewToken(@Context SecurityContext securityContext) {
		Appuser user = AppSecurityContext.user(securityContext);
		return response(app.authentication().createToken(user, Optional.empty()));
	}

	private AuthenticationResponse response(AuthenticationToken auth) {
		return new AuthenticationResponse(auth.getUsername(), auth.getExpiration(), StringUtil.firstToUpperCase(auth.getRole().name()), auth.getToken());
	}
}
