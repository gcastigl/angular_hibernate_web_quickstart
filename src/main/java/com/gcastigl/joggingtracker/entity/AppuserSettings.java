package com.gcastigl.joggingtracker.entity;

import static java.util.Objects.requireNonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "appuser_settings")
public class AppuserSettings {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;

	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "USER_ID", unique = true, nullable = false)
	private Appuser user;

	@Column(name = "EXPECTED_WEEKLY_SPEED", nullable = false)
	private int expectedWeeklySpeed;

	AppuserSettings() {
		// Required by entity
	}

	AppuserSettings(Appuser user) {
		this.user = requireNonNull(user);
	}

	public int getExpectedWeeklySpeed() {
		return expectedWeeklySpeed;
	}
	
	public void setExpectedWeeklySpeed(int expectedWeeklySpeed) {
		this.expectedWeeklySpeed = expectedWeeklySpeed;
	}
}
