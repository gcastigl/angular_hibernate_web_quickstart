package com.gcastigl.joggingtracker.entity;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.gcastigl.joggingtracker.entity.Role.RoleType;

public class AuthenticationToken {

	private String username;
	private Long expiration;
	private RoleType role;
	private String token;

	public AuthenticationToken(String username, Long expiration, RoleType role, String token) {
		this.username = username;
		this.expiration = expiration;
		this.role = role;
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getExpiration() {
		return expiration;
	}

	public void setExpiration(Long expiration) {
		this.expiration = expiration;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public RoleType getRole() {
		return role;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
