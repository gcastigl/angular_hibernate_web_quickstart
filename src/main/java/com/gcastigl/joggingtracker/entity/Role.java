package com.gcastigl.joggingtracker.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role {

	public static enum RoleType {
		ADMIN, REGULAR
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;

	@Column(name = "NAME", unique = true, nullable = false)
	@Enumerated(EnumType.STRING)
	private RoleType name;

	Role() {
		// Required by entity
	}

	public Role(RoleType name) {
		this.name = name;
	}

	public RoleType getName() {
		return name;
	}
}
