package com.gcastigl.joggingtracker.entity;

import java.io.Serializable;

public interface PersistentEntity<T extends Serializable> {

	T getId();

}
