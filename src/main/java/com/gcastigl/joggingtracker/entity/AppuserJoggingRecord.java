package com.gcastigl.joggingtracker.entity;

import static java.util.Objects.requireNonNull;

import java.time.LocalDate;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import jersey.repackaged.com.google.common.base.Preconditions;

@Entity
@Table(name = "appuser_jogging_record")
public class AppuserJoggingRecord implements PersistentEntity<Long> {

	public static final int COMMENT_MAX_LEN = 255;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "USER_ID", nullable = false)
	private Appuser user;

	@Column(name = "DATE", nullable = false)
	private LocalDate date;

	@Column(name = "DISTANCE", nullable = false)
	private float distance;

	@Column(name = "DURATION", nullable = false)
	private float duration;

	@Column(name = "COMMENT", length = COMMENT_MAX_LEN)
	private String comment;

	AppuserJoggingRecord() {
		// Required by entity
	}

	public AppuserJoggingRecord(Appuser user, LocalDate date, float distance, float duration, Optional<String> comment) {
		this.user = requireNonNull(user);
		this.date = requireNonNull(date);
		this.distance = distance;
		this.duration = duration;
		this.comment = comment.orElse(null);
		Preconditions.checkArgument(this.distance > 0);
		Preconditions.checkArgument(this.duration > 0);
		Preconditions.checkArgument(getComment() == null || getComment().length() < COMMENT_MAX_LEN);
	}

	@Override
	public Long getId() {
		return id;
	}

	public LocalDate getDateTime() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public float getDuration() {
		return duration;
	}

	public void setDuration(float duration) {
		this.duration = duration;
	}

	public Appuser getUser() {
		return user;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
