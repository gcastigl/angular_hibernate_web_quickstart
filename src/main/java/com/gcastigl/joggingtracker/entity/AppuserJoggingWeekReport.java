package com.gcastigl.joggingtracker.entity;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AppuserJoggingWeekReport {

	private int year;
	private int weekNumber;
	private double totalDistance;
	private double averageSpeed;
	private long joggingTimes;
	private String username;

	public AppuserJoggingWeekReport(int year, int weekNumber, double totalDistance, double averageSpeed, long joggingTimes, String username) {
		this.year = year;
		this.weekNumber = weekNumber;
		this.totalDistance = totalDistance;
		this.averageSpeed = averageSpeed;
		this.joggingTimes = joggingTimes;
		this.username = username;
	}

	public int getWeekNumber() {
		return weekNumber;
	}

	public double getTotalDistance() {
		return totalDistance;
	}

	public double getAverageSpeed() {
		return averageSpeed;
	}

	public long getJoggingTimes() {
		return joggingTimes;
	}

	public String getUsername() {
		return username;
	}

	public int getYear() {
		return year;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

}
