package com.gcastigl.joggingtracker.entity;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "appuser")
public class Appuser implements PersistentEntity<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;

	@Column(name = "USERNAME", unique = true, nullable = false)
	private String username;

	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@Column(name = "ENABLED", nullable = false)
	private boolean enabled;

	@Column(name = "EMAIL")
	private String email;

	@ManyToOne(optional = false)
	@JoinColumn(name = "ROLE_ID", nullable = false)
	private Role role;

	@OneToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.ALL, mappedBy = "user")
	private AppuserSettings settings;

	Appuser() {
		// Required by entity
	}

	public Appuser(String username, String password, Role role) {
		this.username = Objects.requireNonNull(username);
		this.password = Objects.requireNonNull(password);
		this.enabled = true;
		this.role = Objects.requireNonNull(role);
		this.settings = new AppuserSettings(this);
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public AppuserSettings getSettings() {
		return settings;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Appuser)) {
			return false;
		}
		return getUsername().equals(((Appuser) obj).getUsername());
	}

	@Override
	public int hashCode() {
		return getUsername().hashCode();
	}
}
