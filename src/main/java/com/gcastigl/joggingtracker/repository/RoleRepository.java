package com.gcastigl.joggingtracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gcastigl.joggingtracker.entity.Role;
import com.gcastigl.joggingtracker.entity.Role.RoleType;

public interface RoleRepository extends JpaRepository<Role, Integer> {

	Role findByName(RoleType name);

}
