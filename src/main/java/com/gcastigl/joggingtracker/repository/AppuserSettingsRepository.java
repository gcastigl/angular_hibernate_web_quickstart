package com.gcastigl.joggingtracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gcastigl.joggingtracker.entity.AppuserSettings;

public interface AppuserSettingsRepository extends JpaRepository<AppuserSettings, Integer> {

	@Query("SELECT settings FROM AppuserSettings settings WHERE settings.user.username = :username")
	AppuserSettings findByUsername(@Param("username") String username);

}
