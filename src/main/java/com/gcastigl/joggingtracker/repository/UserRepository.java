package com.gcastigl.joggingtracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gcastigl.joggingtracker.entity.Appuser;

public interface UserRepository extends JpaRepository<Appuser, Long> {

	Appuser findByUsername(String username);

	@Query("SELECT password FROM Appuser WHERE username = :username")
	String findPasswordByUsername(@Param("username") String username);

}
