package com.gcastigl.joggingtracker.repository;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gcastigl.joggingtracker.entity.Appuser;
import com.gcastigl.joggingtracker.entity.AppuserJoggingRecord;
import com.gcastigl.joggingtracker.entity.AppuserJoggingWeekReport;

public interface AppuserJoggingRecordRepository extends JpaRepository<AppuserJoggingRecord, Long> {

	@Query(
		"SELECT record FROM AppuserJoggingRecord record"
		+ "	WHERE (:user IS NULL OR record.user = :user)"
		+ "		AND (:from IS NULL OR :from <= record.date)"
		+ "		AND (:to IS NULL OR record.date <= :to)"
		+ "		AND (:username IS NULL OR LOWER(record.user.username) LIKE LOWER(:username))"
	)
	Page<AppuserJoggingRecord> findAllByUser(
		@Param("user") Appuser user, @Param("from") LocalDate from, @Param("to") LocalDate to, @Param("username") String username, Pageable pageable
	);

	@Query(
		value = "SELECT new com.gcastigl.joggingtracker.entity.AppuserJoggingWeekReport("
		+ "		YEAR(record.date) as year, WEEK(record.date) as week, SUM(record.distance), SUM(record.distance) / SUM(record.duration), COUNT(*), record.user.username"
		+ ")"
		+ "	FROM AppuserJoggingRecord record"
		+ "	WHERE (:user IS NULL OR record.user = :user)"
		+ "		AND (:from IS NULL OR :from <= record.date)"
		+ "		AND (:to IS NULL OR record.date <= :to)"
		+ "		AND (:username IS NULL OR LOWER(record.user.username) LIKE LOWER(:username))"
		+ "	GROUP BY record.user.username, YEAR(record.date), WEEK(record.date)"
		+ " ORDER BY year DESC, week DESC"
	)
	Page<AppuserJoggingWeekReport> getWeeklyReport(
		@Param("user") Appuser user, @Param("from") LocalDate from, @Param("to") LocalDate to, @Param("username") String username, Pageable pageable
	);

}
