'use strict';

// declare modules
angular.module('JoggingRecord', []);
angular.module('Authentication', []);
angular.module('User', []);

angular.module('app', [
	'JoggingRecord',
	'Authentication',
	'User',
	'ngRoute',
	'ngCookies',
	'ui.bootstrap',
	'angularCSS',
	'pascalprecht.translate'
])

.config([
	'$routeProvider', '$httpProvider', '$translateProvider',
    function ($routeProvider, $httpProvider, $translateProvider) {
	    $routeProvider
	        .when('/login', {
	            controller: 'LoginController',
	            templateUrl: 'modules/authentication/views/login.html',
	            hideMenus: true,
	            css: 'css/login.css'
	        })
	        .when('/', {
	            templateUrl: 'modules/joggingrecord/views/recordstable.html'
	        })
	        .when('/home', {
	    		templateUrl: 'modules/joggingrecord/views/recordstable.html'
	        })
	        .when('/report', {
	            templateUrl: 'modules/joggingrecord/views/weeklyreport.html'
	        })
	        .when('/account', {
	    		templateUrl: 'modules/user/views/account.html'
	        })
	        .when('/logout', {
	        	controller: 'LogoutController',
	    		templateUrl: 'modules/joggingrecord/views/recordstable.html'
	        })
	        .otherwise({ redirectTo: '/login' });
	
	    $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
	        return {
	        	'responseError': function(rejection) {
	        		var status = rejection.status;
	        		var config = rejection.config;
	        		var method = config.method;
	        		var url = config.url;
	        		if (status == 401) {
	        			$location.path( "/login" );
	        		} else {
	        			$rootScope.error = method + " on " + url + " failed with status " + status;
	        		}
	        		return $q.reject(rejection);
	        	}
	        };
	    });
	    
	    $httpProvider.interceptors.push(function ($q, $rootScope, $location, $injector) {
	        return {
	        	'request': function(config) {
	        		var isRestCall = config.url.indexOf('api') > 0;
	        		if (isRestCall && angular.isDefined($rootScope.globals.currentUser)) {
	        			$injector.get('AuthenticationService').renewTokenIfNeeded(config.url);
//	        			var authToken = $rootScope.globals.currentUser.token;
//	        			config.headers['Authorization'] = "Basic " + authToken;
	        		}
	        		return config || $q.when(config);
	        	}
	        };
	    });
	    
	    $translateProvider.translations('en', {
	    	AuthenticationErrorType_INVALID_CREDENTIALS: 'Invalid username and password',
	    	AuthenticationErrorType_USER_DISABLED: 'The account is disabled. Contact your administrator.',
	    	AuthenticationErrorType_INVALID_TOKEN: 'Invalid token',
	    	UserServiceErrorType_USERNAME_INVALID: 'Username is invalid. Must have at least 4 characteres',
	    	UserServiceErrorType_USERNAME_EXISTS: 'Username already exists', 
	    	UserServiceErrorType_USERNAME_NOT_EXISTS: 'Username not exists', 
	    	UserServiceErrorType_PASSWORD_MISSMATCH: 'Passwords do not match',
	    	UserServiceErrorType_PASSWORD_WEAK: 'Password is too weak. Must have at least 5 characteres',
	    	UserServiceErrorType_PASSWORD_INCORRECT: 'The password is incorrect',
	    	UserServiceErrorType_CANT_EDIT_SELF: 'Can not edit self',
	    	UserServiceErrorType_NOT_ALLOWED: 'Action not allowed',
	    	UserServiceErrorType_ROLE_INVALID: 'Role is invalid',
	    	AppuserJoggingRecordErrorType_INVALID_DISTANCE: 'Distance must be a positive value',
    		AppuserJoggingRecordErrorType_INVALID_DURATION: 'Duration must be a positive value',
    		AppuserJoggingRecordErrorType_INVALID_COMMENT: 'Comment can not exceed 255 characters',
    		AppuserJoggingRecordErrorType_NOT_ALLOWED: 'Action not allowed'
	    });
	    $translateProvider.preferredLanguage('en');
	    $translateProvider.useSanitizeValueStrategy('escape');
	}
])

.run(['$rootScope', '$location', '$cookieStore', '$http', 'AuthenticationService',
    function ($rootScope, $location, $cookieStore, $http, AuthenticationService) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.token;
        }
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !AuthenticationService.isLoggedIn()) {
				$location.path('/login');
            }
        });
    }
])

.directive("appDatePicker", [function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
        	var value = scope.$eval(attrs.ngModel);
        	var selectedDate = value ? new Date(value) : null;
            $(elem).datepicker({
                autoclose: true,
                format: "yyyy-mm-dd",
                setDate: selectedDate
            });
            $(elem).datepicker("update", selectedDate);
        }
    }
}])

.controller('NavController', 
	['$scope', '$rootScope', '$location', 'AuthenticationService', 
	 function($scope, $rootScope, $location, AuthenticationService) {
		$scope.isLoggedIn = function() {
			return AuthenticationService.isLoggedIn();
		}
		$scope.user = $rootScope.globals.currentUser;
		$scope.$on('loginSuccess', function (event, arg) { 
			$scope.user = $rootScope.globals.currentUser;
		});
		
		$scope.isActive = function (viewLocation) { 
			return viewLocation === $location.path();
   		};
	}
])
;
