'use strict';
 
angular.module('JoggingRecord')

.factory('JoggingRecordService',
    ['$http', '$cookieStore', '$rootScope', '$timeout', 'AuthenticationService', 
    function ($http, $cookieStore, $rootScope, $timeout, AuthenticationService) {
        var service = {};

        service.get = function(pageable, filter, callback) {
        	if (AuthenticationService.isLoggedIn()) {
        		filter = filter || {};
        		$http.get('/api/joggingrecords', {
        			params: { 
        				dateFrom: service.formattedDate(filter.dateFrom),
        				dateTo: service.formattedDate(filter.dateTo),
        				username: filter.username,
        				page: pageable.page, 
        				pagesize: pageable.pageSize 
        			}
        		}).success(function (response) {
        			callback(response);
        		});
        	}
        };
        
        service.create = function(record, callback) {
    		$http.post('/api/joggingrecords/create/', {
    			date: service.formattedDate(record.date), 
    			distance: record.distance, 
    			duration: record.duration,
    			comment: record.comment
    		}).success(function (response) {
    			callback(response);
    		}).error(function(response){
    			callback({status : -1});
    		});
        }

        service.update = function(record, callback) {
    		$http.post('/api/joggingrecords/update/', {
    			id: record.id,
    			date: service.formattedDate(record.date), 
    			distance: record.distance, 
    			duration: record.duration,
    			comment: record.comment
    		}).success(function (response) {
    			callback(response);
    		}).error(function(response){
    			callback({status : -1});
    		});
        };

        service.delete_ = function(record, callback) {
        	$http.post('/api/joggingrecords/delete/' + record.id, {
        	}).success(function (response) {
    			callback(response);
    		}).error(function(response){
    			callback({status : -1});
    		});
        };

        service.getWeeklyReport = function(pageable, filter, callback) {
        	if (AuthenticationService.isLoggedIn()) {
        		filter = filter || {};
        		$http.get('/api/joggingrecords/report/', {
        			params: {
        				dateFrom: service.formattedDate(filter.dateFrom),
        				dateTo: service.formattedDate(filter.dateTo),
        				username: filter.username,
        				page: pageable.page, 
        				pagesize: pageable.pageSize
        			}
        		}).success(function (response) {
        			callback(response);
        		}).error(function(response){
        			callback({status : -1});
        		});
        	}
        };

        service.formattedDate = function(date) {
        	return date ? date.toLocaleFormat('%Y-%m-%d') : date;
        };

        return service;
    }
]);
