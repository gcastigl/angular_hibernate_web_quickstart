'use strict';

angular.module('JoggingRecord')

.controller('JoggingRecordController', [
	'$rootScope', '$scope', '$translate', 'JoggingRecordService', 'UserService',
	function($rootScope, $scope, $translate, JoggingRecordService, UserService) {

		$scope.translate = function(key) {
			$translate.use(key);
		};

		$scope.pageable = {page: 0, pageSize: 10};
		$scope.filter = {dateFrom: null, dateTo: null, username: null};
		$scope.previousFilter = angular.copy($scope.filter);
		$scope.pageInfo = {};

		$scope._loadRecords = function() {
			$scope.loading = true;
			if (!angular.equals($scope.previousFilter, $scope.filter)) {
				angular.copy($scope.filter, $scope.previousFilter);
				$scope.pageable.page = 0;
			}
			JoggingRecordService.get($scope.pageable, $scope.filter, function(response) {
				$scope.loading = false;
				$scope.pageInfo = response.pageInfo;
				$scope.joggingRecords = response.content.map(function(record) {
					record.date = new Date(record.date + " 0:0:0");
					return {
						master: record,
						clone: angular.copy(record)
					};
				});
			});
		};

		$scope.newRecord = {
			id: "-1", date: new Date().toLocaleFormat('%Y-%m-%d')
		};

		$scope._loadRecords();

		$scope.create = function(record) {
			JoggingRecordService.create(record, function(response) {
				$scope.storeResponse(record, response);
			});
		}

		$scope.update = function(record) {
			$scope.updating = true;
			JoggingRecordService.update(record.clone, function(response) {
				$scope.updating = false;
				$scope.storeResponse(record.clone, response);
				if ($scope.isSuccess(record.clone)) {
					angular.copy(record.clone, record.master);
				}
			});
		}

		$scope.delete_ = function(record) {
			JoggingRecordService.delete_(record, function(response) {
				$scope.storeResponse(record, response);
			});
		}

		$scope.storeResponse = function(record, response) {
			if (!$scope.updated) {
				$scope.updated = {};
			}
			$scope.updated[record.id] = response;
		}

		$scope.isSuccess = function(record) {
			return $scope.updated && $scope.updated[record.id] && $scope.updated[record.id].status === 0;
		}

		$scope.hasError = function(record) {
			return $scope.updated && $scope.updated[record.id] && $scope.updated[record.id].status != 0;
		}

		$scope.createModal = function(modalId, cloneRecord, masterRecord) {
			var modal = $(modalId);
			modal.on('hidden.bs.modal', function () {
				if ($scope.isSuccess(cloneRecord)) {
					$scope._loadRecords();
					$scope._reloadWeeklyReport();
				}
				if ($scope.updated) {
					$scope.updated[cloneRecord.id] = null;
				}
			});
			if (masterRecord != null && cloneRecord != null && masterRecord !== cloneRecord) {
				angular.copy(masterRecord, cloneRecord);
			}
			modal.modal('toggle');
		}

		$scope.filterResults = function() {
			$scope._loadRecords();
		};

		$scope._reloadWeeklyReport = function() {
			$rootScope.$broadcast('recordUpdated', '');
		}

		$scope.nextPage = function() {
			$scope.pageable.page++;
			$scope._loadRecords();
		}

		$scope.previousPage = function() {
			$scope.pageable.page =  Math.max(0, $scope.pageable.page - 1);
			$scope._loadRecords();
		}
		
		$scope.isAdmin = UserService.isAdmin();
	}

])

.controller('WeeklyReportController', [
	'$rootScope', '$scope', 'JoggingRecordService', 'UserService', 
	function($rootScope, $scope, JoggingRecordService, UserService) {

		$scope.pageable = {page: 0, pageSize: 10};
		$scope.filter = { dateFrom: null, dateTo: null, username: null };
		$scope.previousFilter = angular.copy($scope.filter);
		$scope.pageInfo = {};

		$scope._loadRecords = function() {
			$scope.loading = true;
			if (!angular.equals($scope.previousFilter, $scope.filter)) {
				angular.copy($scope.filter, $scope.previousFilter);
				$scope.pageable.page = 0;
			}
			JoggingRecordService.getWeeklyReport($scope.pageable, $scope.filter, function(response) {
				$scope.loading = false;
				$scope.pageInfo = response.pageInfo;
				$scope.weeklyReports = response.content;
			});
		}
		$scope._loadRecords();

		
		$scope.filterResults = function() {
			$scope._loadRecords();
		};

		$scope.getStartDateOfWeek = function(year, weekNumber) {
			var date = new Date(new Date(year, 0, 1, 1, 0, 0).getTime() + weekNumber * 1000 * 60 * 60 * 24 * 7);
			var start = new Date(date.getTime() - date.getDay() * 1000 * 60 * 60 * 24);
			return JoggingRecordService.formattedDate(start);
		}

		$scope.getEndDateOfWeek = function(year, weekNumber) {
			return $scope.getStartDateOfWeek(year, weekNumber + 1);
		}

		$scope.$on('recordUpdated', function(event, arg) { 
			$scope._loadRecords();
		});
		
		$scope.nextPage = function() {
			$scope.pageable.page++;
			$scope._loadRecords();
		}

		$scope.previousPage = function() {
			$scope.pageable.page =  Math.max(0, $scope.pageable.page - 1);
			$scope._loadRecords();
		}
		
		$scope.isAdmin = UserService.isAdmin();
	}
]);
