'use strict';
 
angular.module('User')
 
.controller('UserController',
    ['$scope', '$rootScope', '$location', 'UserService',
    function ($scope, $rootScope, $location, UserService) {

    	$scope.currUser = $rootScope.globals.currentUser;
    	
    	$scope._loadUsers = function() {
        	UserService.findAll(function(response) {
        		$scope.users = response.map(function(user) {
    				return {
    					master: user,
    					clone: angular.copy(user)
    				};
    			});
        	});    		
    	};
    	$scope._loadUsers();

    	$scope.isAdmin = function() {
    		return UserService.isAdmin();
    	}

    	$scope.roles = UserService.getRoles();

    	$scope.userUpdate = {};
    	$scope.update = function(user) {
    		UserService.update(user.master.username, user.clone.role, user.clone.enabled, function(response) {
    			if (response.status != 0) {
    				user.clone.role = user.master.role;
    				user.clone.enabled = user.master.enabled;
    				$scope.userUpdate.error = response.message;
    			} else {
    				user.master.role = user.clone.role;
    				user.master.enabled = user.clone.enabled;
    				$scope.userUpdate.error = null;
    			}
    		});
    	}
    	
		$scope.$on('userCreated', function (event, arg) { 
			$scope._loadUsers();
		});
    }]
)

.controller('UserEditController',
    ['$scope', '$rootScope', '$location', 'UserService',
    function ($scope, $rootScope, $location, UserService) {
    	$scope.passwordForm = {};
    	$scope.changePassword = function(currPassword, newPassword, newPasswordConfirm) {
    		if (newPassword != newPasswordConfirm) {
    			$scope.passwordForm.error = "Passwords do not match";
    			return;
    		}
    		UserService.setPassword(currPassword, newPassword, function(response) {
    			if (response.result.status != 0) {
    				$scope.passwordForm.error = response.result.message;
    				$scope.passwordForm.success = null;
    			} else {
    				$scope.passwordForm.error = null;
    				$scope.passwordForm.success = true;
    			}
    		});
    	}

    }]
)

.controller('CreateUserController',
	['$scope', '$rootScope', '$location', 'UserService',
	function ($scope, $rootScope, $location, UserService) {

		$scope.newUser = {};
		
		$scope.roles = UserService.getRoles();

		$scope.createUserModal = function(modalId, userRecord) {
			var modal = $(modalId);
			modal.on('hidden.bs.modal', function () {
				$scope.error = null;
				if ($scope.newUser.success) {
					$rootScope.$broadcast('userCreated', '');
				}
				$scope.newUser.success = null;
			});
			modal.modal('toggle');
		};

		$scope.create = function(newUser) {
			UserService.register(newUser.username, newUser.password, newUser.password, newUser.role, function(response) {
				if (response.result.status != 0) {
					$scope.error = response.result.message;
					newUser.success = null;
				} else {
					$scope.error = null;
					newUser.success = true;
				}
        	});
		}

	}]
)
;
