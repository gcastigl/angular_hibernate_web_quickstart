'use strict';

angular.module('User')

.factory('UserService',
	['$http', '$cookieStore', '$rootScope', '$timeout', 'AuthenticationService', 
	function($http, $cookieStore, $rootScope, $timeout, AuthenticationService) {
		var service = {};

        service.register = function(username, password, passwordConfirmation, role, callback) {
            $http.post('/api/users/create', {
            		username: username, 
            		password: password,
            		passwordConfirmation: passwordConfirmation,
            		role: role
            	}).success(function (response) {
	                callback(response);
	            }).error(function(response) {
	            	callback(response);
	            });
        };

		service.getSettings = function(callback) {
			$http.get('/api/settings', {
				
			}).success(function(response) {
				callback(response);
			});
		};

		service.findAll = function(callback) {
			if (service.isAdmin()) {
				$http.get('/api/users', {
					
				}).success(function(response) {
					callback(response);
				});
			}
		}

		service.setPassword = function(currPassword, newPassword, callback) {
			var user = AuthenticationService.user();
			$http.post('/api/users/changepassword', {
				username: user.username,
				currentPassword: currPassword,
				newPassword: newPassword
			}).success(function(response) {
				if (response.result.status == 0) {
					console.log("Token updated!");
					AuthenticationService.setAuth(response);
				}
				callback(response);
			});
		}

		service.update = function(username, role, enabled, callback) {
			$http.post('/api/users/update', {
				username: username,
				role: role,
				enabled: enabled
			}).success(function(response) {
				callback(response);
			}).error(function(response) {
				callback({status: -1, message: 'Server error'});
			});
		}

		service.isAdmin = function() {
    		return $rootScope.globals.currentUser && $rootScope.globals.currentUser.role.toLowerCase() == "admin";
    	}

		service.adminRole = function() {
			return "Admin";
		}
		service.regularRole = function() {
			return "Regular";
		}
		service.getRoles = function() {
    		return [service.adminRole(), service.regularRole()];
    	}

		return service;
	}]
);
