'use strict';
 
angular.module('Authentication')

.factory('AuthenticationService',
    ['$http', '$cookieStore', '$rootScope', '$timeout',
    function ($http, $cookieStore, $rootScope, $timeout) {
        var service = {};

        service.login = function (username, password, callback) {
            $http.post('/api/authenticate/token', { username: username, password: password })
                .success(function (response) {
                    callback(response);
                });
        };

        service.setAuth = function (auth) {
            $rootScope.globals = {
                currentUser: {
                    username: auth.username.toLowerCase(),
                    token: auth.token,
                    expiration: auth.expiration,
                    role: auth.role
                }
            };
            $http.defaults.headers.common['Authorization'] = 'Basic ' + auth.token;
            document.cookie = "globals=" + escape(JSON.stringify($rootScope.globals));
        };
 
        service.clearAuth = function () {
            $rootScope.globals = {};
            document.cookie = "globals=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            document.cookie = "JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            $http.defaults.headers.common.Authorization = 'Basic ';
        };

        service.isLoggedIn = function() {
			var cookie = $cookieStore.get('globals');
			if (!!cookie) {
				var expireTime = (new Date(parseInt(cookie.currentUser.expiration)) - new Date());
				return expireTime > 0;
			}
			return false;
        };

        service.getAuthToken = function() {
        	$rootScope.globals.currentUser.token;
        };

        service.user = function() {
        	return $rootScope.globals.currentUser;
        };

        service.renewTokenIfNeeded = function(url) {
        	if (url.indexOf('renewtoken') == -1) {
        		var cookie = $cookieStore.get('globals');
        		if (!!cookie) {
        			var expireTime = (new Date(parseInt(cookie.currentUser.expiration)) - new Date()) / 1000 / 60;
        			if (expireTime < 30) {
        				console.log("Token about to expire. Requesting renewal.");
						$http.post('/api/authenticate/renewtoken', { })
							.success(function (response) {
			                	if (response.result.status == 0) {
			                		service.setAuth(response);
			                	}
			                });
        			}
        		}
        	}
        }

        return service;
    }
]);
