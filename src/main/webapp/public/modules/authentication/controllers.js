'use strict';
 
angular.module('Authentication')
 
.controller('LoginController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService', 'UserService',
    function ($scope, $rootScope, $location, AuthenticationService, UserService) {

		$scope.changeLanguage  = function(key) {
			$translate.use(key);
		};

        $scope.login = function () {
            $scope.loginDataLoading = true;
            AuthenticationService.login($scope.username, $scope.password, function(response) {
            	$scope.loginDataLoading = false;
                if (response.result.status == 0) {
                    AuthenticationService.setAuth(response);
                    $location.path('/');
                    $rootScope.$broadcast('loginSuccess', response);
                } else {
                    $rootScope.$broadcast('loginError', response);
                }
            });
        };

        $scope.register = function() {
        	$scope.registrationDataLoading = true;
        	UserService.register($scope.newUsername, $scope.newPassword, $scope.newPasswordConfirmation, 'Regular', function(response) {
        		$scope.registrationDataLoading = false;
        		if (response.result.status == 0) {
                    AuthenticationService.setAuth(response);
                    $location.path('/');
                    $rootScope.$broadcast('loginSuccess', response);
        		} else {
        			$scope.registerError = response.result.message;
        		}
        	});
        }
    }]
)

.controller('LoginErrorController',
	['$scope', '$rootScope', '$location', 'AuthenticationService',
	function($scope, $rootScope, $location, AuthenticationService) {

		$scope.$on('loginError', function (event, arg) { 
			$scope.loginError = arg;
		});

	}]
)

.controller('LogoutController', 
	['$scope', '$rootScope', '$location', 'AuthenticationService',
	function($scope, $rootScope, $location, AuthenticationService) {
		AuthenticationService.clearAuth();
		$location.path('/login');
	}]
)
;
