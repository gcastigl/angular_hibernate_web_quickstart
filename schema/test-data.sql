
INSERT INTO role(ID, NAME) VALUES 
	(1, "ADMIN"),
	(2, "REGULAR")
;

INSERT INTO appuser(ID, USERNAME, PASSWORD, ENABLED, ROLE_ID) VALUES
	(1, "dev", "$2a$10$FPoHJ02UxJPIfMa/tqDTm.6.ucTDFpRQIj3JLPZnmp9NqajSggS4W", 1, 1)
;

INSERT INTO appuser_settings(ID, USER_ID, EXPECTED_WEEKLY_SPEED) VALUES
	(1, 1, 10)
;

INSERT INTO appuser_jogging_record(USER_ID, DATE, DISTANCE, DURATION) VALUES
	(1, "2016-04-01", 1.5, 1),
	(1, "2016-04-01", 1.3, 0.4),
	(1, "2016-04-01", 4, 2)
;

insert into appuser_jogging_record(DATE, DISTANCE, DURATION, USER_ID) VALUES
	(CONCAT('2016', "-", FLOOR(RAND() * 3) + 4, "-", FLOOR(RAND() * 28) + 1), RAND() * 4 + 2, RAND() * 5, 1)
;
