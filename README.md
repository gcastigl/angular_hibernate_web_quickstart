# Information
This project is a __quick start template__ for __Java Web__ project development.

## Technologies
	1. Maven - Dependency management
	2. JPA (Hibernate) - Persistance layer
	3. Spring - Dependency injection, etc.
	4. JUnit, hamcreat - Testing
	5. Jersey - Web server controller
	6. AngularJS - Web View
	7. Log4j - Logging
	8. Spring Boot - Servlet engine

# Development

## IDE
	1. Copy contents from src/test/resources/application.properties.tpt into src/main/resources/application.properties and configure as desired
	2. Run Application.java class

## Console
	1. Copy contents from src/test/resources/application.properties.tpt into src/main/resources/application.properties and configure as desired
	2. Execute: mvn spring-boot:run

# Deployment
To run web application:

	1. Copy contents from src/test/resources/application.properties.tpt into src/main/resources/application.properties and configure as desired
	2. mvn clean package
	3. Deploy

# Testing
To run all tests:

	1. Execute: mvn clean test

